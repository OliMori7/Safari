﻿<%@ Page Title="About Us" Language="C#" MasterPageFile="~/Safari.Master" AutoEventWireup="true" CodeBehind="pageAboutUs.aspx.cs" Inherits="Labo4_Safari.pageAboutUs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="bg">
        <div id="wholeUs">
            <div id="centraleAu">
                <br />
                <h1>About us</h1>
                __________________________________________________________________________
                <p>Travel throughout Africa with the Safari Adventure Company. Unrivalled
                experience, unlimited knowledge and a passion for exploring Africa.</p>
                <asp:Button ID="butRond" CssClass="butRond" runat="server" Text="MORE" />
            </div>
        <div id="">
            <br />
            <h2>Why choose us?</h2>

        </div>
    <div id="textInfoUs">
                    <div class="textUs">
                        <h4>New Impressions</h4>
                        <p>An African safari isn't a passive experience. It isn't just about
                        glimpsing wildlife. You're enveloped in their untamed world, absolutely
                        surronded by the drama and charm of the planet's greatest theater,
                        every angle is unique and each moment personalized.</p>
                        <asp:Button ID="Button1" CssClass="boutonUs" runat="server" Text="More" />
                    </div>
                    <div class="textUs">
                        <h4>Best prices</h4>
                        <p>We offer a range of safaris, tours and excursions at affordable
                        prices to individuals, couples, families and groups. We also offer
                        airport transfers (at a small fee) and booking of safari camps and
                        lodges for our clients. Low prices are the cornerstone of our vision.</p>
                        <asp:Button ID="Button2" CssClass="boutonUs" runat="server" Text="More" />
                    </div>
                    <div class="textUs">
                        <h4>Best Camping Conditions</h4>
                        <p>Our wide range safari vacations to the different destinations and wildlife
                        areas in Africa enable you to plan the ideal holiday in Africa. We try
                        out many of these safari vacations on a regular basis to ensure the
                        highest standards are maintained ans offered travelers.</p>
                        <asp:Button ID="Button3" CssClass="boutonUs" runat="server" Text="More" />
                    </div>
                </div>
            </div>
        </div>
</asp:Content>
