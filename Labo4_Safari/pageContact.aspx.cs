﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Labo4_Safari
{
    public partial class pageContact : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void butClear_Click(object sender, EventArgs e)
        {
            txtName.Text = "";
            txtEmail.Text = "";
            txtPhone.Text = "";
            txtMessage.Text = "";
        }

        protected void butSend_Click(object sender, EventArgs e)
        {
            if (rfvName.IsValid)
            {
                if(emailValidator.IsValid)
                { 
                    txtMessage.Text = "Thank you!";
                }
                else
                {
                    txtMessage.Text = emailValidator.ErrorMessage + " ";
                }
            }
            else
            {
                txtMessage.Text = rfvName.ErrorMessage + " ";
            }
            
        }
    }
}