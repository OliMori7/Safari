﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Safari.Master" AutoEventWireup="true" CodeBehind="pageContact.aspx.cs" Inherits="Labo4_Safari.pageContact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="bgContact">
        <div id="infoContact">
            <h3>ADDRESS:</h3>
            <p><img src="Images/simple-orange-house-hi.png" /> 138 Atlantis Ln Kingsport Illinois 121164</p>
            <h3>PHONES:</h3>
            <p><img src="Images/phone.png" /> 800-2345-6789</p>
            <p><img src="Images/fax.png" /> 800-2345-6789</p>
            <h3>E-MAIL:</h3>
            <p><img src="Images/mail-xxl.png" /> mail@demolink.org</p>
            <p>Download informations as: <a>vCard</a></p>
        </div>
        <div id="miscInfo">
            <h3>MISCELLANEOUS</h3>
            <p>E-mail us with any questions or inquiries or use our contact data.</p>
            <div id="champs">
                <asp:TextBox ID="txtName" runat="server" placeholder="Name" CssClass="champsReq"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="Enter your name" ></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtEmail" runat="server" placeholder="E-mail" CssClass="champsReq"></asp:TextBox>
                <asp:RegularExpressionValidator runat="server" ID="emailValidator" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Enter a valid e-mail address" />
                <asp:TextBox ID="txtPhone" runat="server" placeholder="Phone" CssClass="champsReq"></asp:TextBox>
            </div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List" />
            <div id="message">
            <asp:TextBox ID="txtMessage" CssClass="messErr" runat="server" placeholder="Message" TextMode="MultiLine"></asp:TextBox>
            </div>
            <div id="butInfo">
                <asp:Button ID="butSend" runat="server" Text="Send" CssClass="boutonInfo" OnClick="butSend_Click" />
                <asp:Button ID="butClear" runat="server" Text="Clear" CssClass="boutonInfo" OnClick="butClear_Click" />
            </div>
        </div>
    </div>
</asp:Content>