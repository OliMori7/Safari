﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/Safari.Master" AutoEventWireup="true" CodeBehind="pageAccueil.aspx.cs" Inherits="Labo4_Safari.pageAccueil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="JS/jquery-3.2.0.min.js"></script>
    <script src="JS/banniere.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="centerAccueil">
        <div id="imagesDeroulantes">
            <div id="ads">
                    <h1>Explore Africa</h1>
                <p id="desc">During amazing Safari tours!</p>
            </div>
            <div id="select">
                <div id="select1">
                    allo
                </div>
                <div id="select2">
                    allo
                </div>
                <div id="select3">
                    allo
                </div>
            </div>
            <div id="titres">
                <div class="sousTitres">
                    Unusual Holidays
                </div>
                <div class="sousTitres">
                    Best Time to Visit
                </div>
                <div class="sousTitres">
                    Big Cat Safaris
                </div>
            </div>
        </div>
        <div id="basAccueil">
            <div id="info">
                <div class="photoInfo">
                    <img id="giraffe" src="Images/giraffe.jpg" />
                </div>
                <div class="photoInfo">
                    <img id="rhinos" src="Images/rhinos.jpg" />
                </div>
                <div class="photoInfo">
                    <img id="lionceaux" src="Images/lionceaux.jpg" />
                </div>
                <div id="textInfo">
                    <div class="text">
                        <br />
                        Spend a weekend with us
                        <br />
                        <p>When you book your holiday with Safari Adventure Co, you can rest assured that we are a full licensed travel agent, so your hard earned holiday money is secured.</p>
                        <asp:Button ID="Button1" CssClass="bouton" runat="server" Text="More" />
                    </div>
                    <div class="text">
                        <br />
                        Plan your african Safari with us
                        <br />
                        <p>Our company offers comprehensive information on Africa travel, connecting you with the best travel service providers who will assist with your Africa safari travel arrangement. </p>
                        <asp:Button ID="Button2" CssClass="bouton" runat="server" Text="More" />
                    </div>
                    <div class="text">
                        <br />
                        Explore Africa's unique Safari
                        <br />
                        <p>The variety and beauty of Africa will take your breath away... be it a wildlife safari or a relaxing beach holiday on Zanzibar, the choices are exceptional. </p>
                        <asp:Button ID="Button3" CssClass="bouton" runat="server" Text="More" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
