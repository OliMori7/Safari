﻿<%@ Page Title="Gallery" Language="C#" MasterPageFile="~/Safari.Master" AutoEventWireup="true" CodeBehind="pageGallery.aspx.cs" Inherits="Labo4_Safari.pageGallery" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="JS/jquery-3.2.0.min.js"></script>
    <script src="JS/jsGallery.js"></script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

    <div id="bgGallery">
            <div id="imgGal">
            <h1>Gallery</h1>
            <img id="im1" src="Images/elephants.jpg" />
            <img id="im2" src="Images/badger.jpg" />
            <img id="im3" src="Images/cheetah.jpg" />
            <img id="im4" src="Images/fennec-fox.jpg" />
            <img id="im5" src="Images/monkeys.jpg" />
            <img id="im6" src="Images/zebras.jpg" />
            <img id="im7" src="Images/iguana.jpg" />
            </div>
    </div>
    
    <script src="JS/jquery-3.2.0.min.js"></script>
    <script src="JS/jsGallery.js"></script>
</asp:Content>
