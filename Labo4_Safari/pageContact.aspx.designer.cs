﻿//------------------------------------------------------------------------------
// <généré automatiquement>
//     Ce code a été généré par un outil.
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </généré automatiquement>
//------------------------------------------------------------------------------

namespace Labo4_Safari {
    
    
    public partial class pageContact {
        
        /// <summary>
        /// Contrôle txtName.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtName;
        
        /// <summary>
        /// Contrôle rfvName.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvName;
        
        /// <summary>
        /// Contrôle txtEmail.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtEmail;
        
        /// <summary>
        /// Contrôle emailValidator.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RegularExpressionValidator emailValidator;
        
        /// <summary>
        /// Contrôle txtPhone.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtPhone;
        
        /// <summary>
        /// Contrôle ValidationSummary1.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
        
        /// <summary>
        /// Contrôle txtMessage.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtMessage;
        
        /// <summary>
        /// Contrôle butSend.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button butSend;
        
        /// <summary>
        /// Contrôle butClear.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button butClear;
    }
}
